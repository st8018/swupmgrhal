/*
 * PlatformSoftwareUpgradeManager.cpp
 *
 */

#include <sstream>
#include <string>

#include <unistd.h>

#include "swupMgrInternal.h"
#include "PlatformSoftwareUpgradeManager.h"

//#include "WDHNavReloadReason.h"

PlatformSoftwareUpgradeManager::PlatformSoftwareUpgradeManager ()
{
}

PlatformSoftwareUpgradeManager::~PlatformSoftwareUpgradeManager ()
{
}

void PlatformSoftwareUpgradeManager::initHAL ()
{
    printf("PlatformSoftwareUpgradeManager::initHAL is called\n");
}

void PlatformSoftwareUpgradeManager::deinitHAL ()
{
    printf("PlatformSoftwareUpgradeManager::deinitHAL is called\n");
}

int PlatformSoftwareUpgradeManager::execShellCommand (const std::string& command)
{
    int status = -1;
    printf("invoking command : \'%s\' \n", command.c_str ());

    try
    {
        status = system (command.c_str ());
        printf("system execution returned status : %d\n", status);
    }
    catch (std::exception& e)
    {
        printf("Exception while executing command \'%s\' : %s\n", command.c_str (), e.what ());
    }
    return status;
}

int PlatformSoftwareUpgradeManager::execShellCommandWithOutput (const std::string& command, std::vector<std::string>& output)
{
    std::string outRedirect = command;
    std::string errRedirect = " 2>&1";
    outRedirect.append (errRedirect);
    printf("invoking command with output: %s\n", outRedirect.c_str ());
    try
    {
        FILE* pipe = popen (outRedirect.c_str (), "r");
        if (!pipe)
        {
            printf("Cannot execute command \'%s\', or pipe cannot be created\n", outRedirect.c_str());
            return -1;
        }
        char buffer[256];
        while (!feof (pipe))
        {
            if (fgets (buffer, 256, pipe) != NULL)
                output.push_back(std::string(buffer));
        }
        pclose (pipe);
    }
    catch (std::exception& e)
    {
        printf("Exception while executing command \'%s\' : %s\n", outRedirect.c_str (), e.what ());
        return -1;
    }
    return 0;
}


SoftwareUpgradeManager::DownloadSWImageResult PlatformSoftwareUpgradeManager::downloadSWImage (const std::string& url, const std::string& userName, const std::string& password, const uint32_t fileSize, const std::string& targetFileName)
{
    printf("PlatformSoftwareUpgradeManager::downloadSWImage is called\n");

    printf("url=%s\n", url.c_str());

    // create thread(downloadSWImageThread) and run downloadSWImage.sh
    pthread_t thread;
    if (pthread_create (&thread, NULL, &PlatformSoftwareUpgradeManager::downloadSWImageThread,
                (void*)url.c_str()) != 0) {
        printf("Failed to create downloadSWImageThread\n");
        return DOWNLOAD_SW_IMAGE_NOT_STARTED;
    }

    return DOWNLOAD_SW_IMAGE_STARTED;
}

SoftwareUpgradeManager::InstallSWImageResult PlatformSoftwareUpgradeManager::installSWImage (bool afterReboot)
{
    printf("PlatformSoftwareUpgradeManager::installSWImage is called\n");

    // create thread(installSWImageThread) and run installSWImage.sh
    pthread_t thread;
    if (pthread_create (&thread, NULL, &PlatformSoftwareUpgradeManager::installSWImageThread, (void*)afterReboot) != 0) {
        printf("Failed to create installSWImageThread\n");
        return INSTALL_SW_IMAGE_NOT_STARTED;
    }

    return INSTALL_SW_IMAGE_STARTED;
}

void PlatformSoftwareUpgradeManager::cancelSWUpgrade ()
{
    printf("PlatformSoftwareUpgradeManager::cancelSWUpgrade is called\n");
}

void* PlatformSoftwareUpgradeManager::downloadSWImageThread(void *arg)
{
    SoftwareUpgradeManager::SWUpgradeStateChangeInfo info;
    std::vector<std::string> output;
    char *url = (char *)arg;
    std::string urlStr(url);

    info.state = SWUPGRADE_STATE_FAILED;
    int status = execShellCommandWithOutput("downloadSWImage.sh " + urlStr, output);
    if (status == 0) {
        int size = output.size();
        if (size > 0) {
            std::string result = output.at(size - 1);
            if (strstr(result.c_str(), "Success")) {
                printf("Succeeded in downloadSWImage.sh!!\n");
                info.state = SWUPGRADE_STATE_UNPACKING_IMAGE;
            } else {
                // result is not "Success"
                printf("result of downloadSWImage.sh is not \"Success\n");
                info.faultCode = FAULTCODE_FT_FAILURE_UNABLE_TO_COMPLETE_DOWNLOAD;
                info.faultString = result;
            }
        } else {
            // No output from downloadSWImage.sh
            printf("No output from downloadSWImage.sh\n");
            info.faultCode = FAULTCODE_INTERNAL_ERROR;
            info.faultString = "downloadSWImage.sh returned no result";
        }
    } else {
        // exception thrown
        printf("Exception thrown in downloadSWImage.sh\n");
        info.faultCode = FAULTCODE_REQUEST_DENIED; 
        info.faultString = "Exception thrown in download";
    }

    SoftwareUpgradeManager::instance().setSoftwareUpgradeState(info);

    return NULL;
}

void* PlatformSoftwareUpgradeManager::installSWImageThread(void *arg)
{
    SoftwareUpgradeManager::SWUpgradeStateChangeInfo info;
    std::vector<std::string> output;
    bool isReboot = (bool *)arg;

    info.state = SWUPGRADE_STATE_FLASHING_IMAGE;
    SoftwareUpgradeManager::instance().setSoftwareUpgradeState(info);

    int status = execShellCommandWithOutput("installSWImage.sh", output);

    if (status == 0) {
        int size = output.size();
        if (size > 0) {
            std::string result = output.at(size - 1);
            if (strstr(result.c_str(), "finished 0")) {
                info.state = SWUPGRADE_STATE_COMPLETED;
            } else {
                printf("result of installSWImage.sh is not \"Success\"\n");
                // TODO : is this fault code suitable?
                info.state = SWUPGRADE_STATE_FAILED;
                info.faultCode = FAULTCODE_FT_FAILURE_UNABLE_TO_ACCESS_FILE;
                info.faultString = "Failed in installation";
            }
        } else {
            // No output from installSWImage.sh
            printf("No output from installSWImage.sh\n");
            info.state = SWUPGRADE_STATE_FAILED;
            info.faultCode = FAULTCODE_INTERNAL_ERROR;
            info.faultString = "installSWImage.sh returned no result";
        }
    } else {
        // exception thrown in installSWImage.sh
        printf("Exception thrown in installSWImage.sh\n");
        info.state = SWUPGRADE_STATE_FAILED;
        info.faultCode = FAULTCODE_REQUEST_DENIED;
        info.faultString = "Exception thrown in installation";
    }

    SoftwareUpgradeManager::instance().setSoftwareUpgradeState(info);

    printf("after reboot is [%d]\n", isReboot);

    if (isReboot && info.state == SWUPGRADE_STATE_COMPLETED) {
        std::string cmd = "r.sh ";
        std::ostringstream s;
        s << 6;
        cmd.append(s.str());

        usleep(10*1000*1000);

        info.state = SWUPGRADE_STATE_WAITING_FOR_REBOOT;
        SoftwareUpgradeManager::instance().setSoftwareUpgradeState(info);

        usleep(10*1000*1000);

        status = execShellCommand(cmd);
    }

    return NULL;
}
