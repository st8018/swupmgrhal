#!/bin/sh
cd /lib/rdk
IMAGE_NAME=$1
if [ -z "$IMAGE_NAME" ] ; then
    IMAGE_NAME="PCI.bin"
fi
./flashimage.sh /tmp/system/swupdate/$IMAGE_NAME
res=$?
echo "installSWImage.sh finished $res"
exit $res
