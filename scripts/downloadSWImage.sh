#!/bin/sh
echo "downloadSWImage.sh"
RET=0
USER_ARG=""
TARGET_NAME="PCI.bin"
while getopts ":u:o:" o; do
    case "${o}" in
        u)
            USER_ARG="-u ${OPTARG}"
            ;;
        o)
            TARGET_NAME=${OPTARG}
            ;;
    esac
done
shift $((OPTIND-1))
URL=$1

rm -rf /tmp/system/swupdate
mkdir -p /tmp/system/swupdate
cd /tmp/system/swupdate

#document=`curl -6 $USER_ARG -vg -I $URL | grep "200 OK"`
#if [ -z "$document" ] ; then
#    echo "Invalid URL"
#    exit 1
#fi

#echo "curl -6 $USER_ARG -vg -o $TARGET_NAME $URL"
#curl -6 $USER_ARG -vg -o $TARGET_NAME $URL
echo "wget $URL -P /tmp/system/swupdate -O $TARGET_NAME"
wget $URL -P /tmp/system/swupdate -O $TARGET_NAME
ERROR=$?

if [ ! -f $TARGET_NAME ] ; then
    echo "Can't download software."
    exit 2
fi

if [ $ERROR = 0 ]; then
    echo "Success"
    exit 0
else
    echo "ERROR CODE=$ERROR"
    exit $ERROR
fi
